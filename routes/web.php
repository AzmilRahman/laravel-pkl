<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LatihanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('about',function ()
{ return view ("tentang");
});

Route::get('latihan/{nama?}/{alamat?}/{umur?}', [LatihanController::class,'perkenalan1']);

// use Illuminate\Support\Facedes\Route;

// Route::get('latihan1/{makanan?}/{minuman?}/{cemilan?}/{harga?}',function ($makanan = "-", $minuman = "-", $cemilan = "-", $harga = "-")
// { return view ('pages.latihan1', compact('makanan', 'minuman', 'cemilan','harga'));
//   });

//   Route::get('latihan1/{makanan?}/',function ($makanan = "-",$minuman = "-",$harga = "-")
//  { return view ('pages.latihan1', compact('makanan', 'minuman', 'harga'));
//  });


// Route::get('pemesanan/{makanan}/{minuman}/{harga}',function ($makanan, $minuman, $harga)
//  { return view ('pages.pemesanan', compact('makanan', 'minuman', 'harga'));
//  });

//  Route::get('pemesanan/{makanan?}/',function ($makanan = "-",$minuman = "-",$harga = "-")
// { return view ('pages.pemesanan', compact('makanan', 'minuman', 'harga'));
// });


// Route::get('biodata/{nama}',function ($nama)  
// { return view ('pages.biodata', compact('nama'));
// });

// Route::get('biodata',function ()
// {   $nama = "azmil";
//     $alamat = "Ciparay";
//     $gender = "Lelaki";
//     $kelas = "XII RPl 3";
//     $sekolah = "SMK ASSALAAM";
//     return view ('pages.biodata', compact(
//         'nama',
//         'alamat',
//         'gender',
//         'kelas',
//         'sekolah'

//     ));
// });


  
    // ('pages.biodata', compact('alamat'));
